
//al cargar la pagina
departamentos.setDepartamentos();
//eventos
document.getElementById('enviar').addEventListener('click', function (event) {
    event.preventDefault();
    //validar Datos
    validarDatos();
})
document.querySelector('#departamento').addEventListener('change', function (event) {
    departamentos.setLocalidad(event);
});

//focus
document.getElementById('nombre').addEventListener('focus', function (event) {
    formValidate.deleteshowError('nombre');
});
document.getElementById('apellido').addEventListener('focus', function (event) {
    formValidate.deleteshowError('apellido');
});
document.getElementById('email').addEventListener('focus', function (event) {
    formValidate.deleteshowError('email');
});
document.getElementById('departamento').addEventListener('focus', function (event) {
    formValidate.deleteshowError('departamento');
});
document.getElementById('localidad').addEventListener('focus', function (event) {
    formValidate.deleteshowError('localidad');
});
document.getElementById('ci').addEventListener('focus', function (event) {
    formValidate.deleteshowError('ci');
});
document.getElementById('condiciones').addEventListener('focus', function (event) {
    formValidate.deleteshowError('condiciones');
});


//función para validar el formulario
function validarDatos() {
    // validar nombre y apellido
    var inputs = ['nombre', 'apellido'];
    for (inputName in inputs) {
        var inputValue = document.getElementById(inputs[inputName]).value,
            msj = '* Campo ' + inputs[inputName] + ' es obligatorio y/o debe tener dos caracteres'
        formValidate.validateInputEmpty(inputValue, inputs[inputName], msj);
        formValidate.validateInputLength(inputValue, inputs[inputName], msj);
    }
    // validar email
    var emailvalue = document.getElementById('email').value;
    formValidate.validateEmail(emailvalue, 'email');
    // validar los campos select
    var inputDepLoc = ['departamento', 'localidad'];
    for (iter in inputDepLoc) {
        var input = document.getElementById(inputDepLoc[iter]),
            msj = '* Debe seleccionar un(@) ' + inputDepLoc[iter];
        formValidate.validateSelect(input, inputDepLoc[iter], msj);
    }
    // validar la cedula
    var inputValue = document.getElementById('ci').value,
        msj = '* Campo CI es obligatorio';
    formValidate.validateInputEmpty(inputValue, 'ci', msj);
    formValidate.validateCI('ci');
    //terminos y condiciones
    var isChecked = document.getElementById('condiciones').checked;
    if (!isChecked) {
        document.getElementsByClassName('condiciones')[0].innerHTML = '* Debe aceptar las bases y las condicones';
    }
}


