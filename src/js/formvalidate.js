
var formValidate = {
    validateInputEmpty: function (inputValue, inputName, msj) {
        if (inputValue.length == 0) {
            formValidate.showError(inputName, msj);
            return false;
        }
        return true;
    },
    validateInputLength: function (inputValue, inputName, msj) {
        if (inputValue.length < 2) {
            formValidate.showError(inputName, msj);
            return false;
        }
        return true;
    },
    validateEmail: function (inputValue, inputName) {
        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i,
            msj = '* Error en el email';
        if (!emailRegex.test(inputValue)) {
            formValidate.showError(inputName, msj);
        }
    },
    validateCI: function (input) {
        var cedula = document.getElementById(input).value,
        msj= '* Error en el formato de la cedula';
        if (!validarCedula(cedula)) {
            formValidate.showError(input, msj);
        }
    },
    validateSelect: function(input, inputName, msj){
        if (input.selectedIndex < 1) {
            formValidate.showError(inputName, msj);
        }
    },
    showError(campo, msj) {
        document.getElementsByClassName(campo)[0].innerHTML = msj;
        document.getElementById(campo).style.borderColor = "red";
    },
    deleteshowError(campo) {
        document.getElementsByClassName(campo)[0].innerHTML = "";
        document.getElementById(campo).removeAttribute('style');
    }
}