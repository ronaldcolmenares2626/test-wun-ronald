var departamentos = {
    getDepartamentos: dptosLocs,
    setDepartamentos: function () {
        const selectDepart = document.getElementById('departamento');
        for (const departamento in this.getDepartamentos) {
            departamentos.buildselect(selectDepart, departamento);
        }
    },
    setLocalidad: function (event) {
        departamentos.cleanSelect();
        const selectlocalidad = document.getElementById('localidad');
        var departamentoselect = this.getDepartamentos[event.target.value];
        for (localidad in departamentoselect) {
            departamentos.buildselect(selectlocalidad, departamentoselect[localidad]);
        }
    },
    buildselect: function (selector, value) {
        const option = document.createElement("option");
        option.text = value;
        selector.appendChild(option);
    },
    cleanSelect: function () {
        document.getElementById('localidad').innerHTML = "<option>Localidad</option>";
    }

}